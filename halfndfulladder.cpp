#include<iostream>
using namespace std;

class HalfAdder{
	private:
		int a,b,res,carry;
	public:
		HalfAdder(){
			res=0;
			carry=0;
		}
		friend istream &operator >>(istream &in,HalfAdder &obj){
			cout<<"Enter a: ";
			in>>obj.a;
			cout<<"Enter b: ";
			in>>obj.b;
		}
		friend ostream &operator <<(ostream &out,HalfAdder &obj){
			cout<<"Result = ";
			out<<obj.res<<endl;
			cout<<"Carry = ";
			out<<obj.carry<<endl;
		}
		HalfAdder compute();
};

class FullAdder{
	private:
		int a,b,c,res,carry;
	public:
		FullAdder(){
			res=0;
			carry=0;
		}
		friend istream &operator >>(istream &in,FullAdder &obj){
			cout<<"Enter a: ";
			in>>obj.a;
			cout<<"Enter b: ";
			in>>obj.b;
			cout<<"Enter c: ";
			in>>obj.c;
		}
		friend ostream &operator <<(ostream &out,FullAdder &obj){
			cout<<"Result = ";
			out<<obj.res<<endl;
			cout<<"Carry = ";
			out<<obj.carry<<endl;
		}
		FullAdder compute();
};
FullAdder FullAdder::compute(){
	this->res= a^b^c;
	this->carry=((a^b)&c)|(a&b);
	return *this;
}

HalfAdder HalfAdder::compute(){
	this->res= a^b;
	this->carry=a&b;
	return *this;
}

int main(){
	HalfAdder halfadder,ob1;
	FullAdder fulladder,ob2;
	
	int ch;
	while(1){
		cout<<"1:Half Adder \n2:Full Adder\n 3:Exit\nEnter your choice: ";
		cin>>ch;
		switch(ch){
			case 1:
				cin>>halfadder;
				ob1=halfadder.compute();
				cout<<ob1;
				break;
			case 2:
				cin>>fulladder;
				ob2=fulladder.compute();
				cout<<ob2;
				break;
			case 3:
				exit(0);
			default:
				cout<<"Incorrect choice"<<endl;
		}
	}
	
}